resource "aws_iam_role" "poll_libor_lambda_role" {
  name = "poll-libor-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "LambdaPollLibor"
    }
  ]
}
EOF
}

resource "aws_iam_role" "query_libor_lambda_role" {
  name = "query-libor-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "LambdaQueryLibor"
    }
  ]
}
EOF
}
