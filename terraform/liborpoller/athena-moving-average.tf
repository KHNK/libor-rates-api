resource "aws_athena_database" "rates_db" {
  name   = "libor_rates_db"
  bucket = "${aws_s3_bucket.daily_libor_rates.bucket}"
}
