resource "aws_s3_bucket" "daily_libor_rates" {
  bucket = "liborapi-daily-libor-rates"
  acl    = "private"
  tags   = "${local.default_tags}"
}

resource "aws_s3_bucket" "test_liborapi_bucket" {
  bucket = "test-liborapi-bucket"
  acl    = "private"
  tags   = "${local.default_tags}"
}
