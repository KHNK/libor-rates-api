variable default_tags {
    type = "map"
    default {
        project = "libor-rates-api"
    }
}
locals {
    default_tags = "${var.default_tags}" 
}