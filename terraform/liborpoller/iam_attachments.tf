resource "aws_iam_role_policy_attachment" "poll_libor_attach_s3_policy" {
  role       = "${aws_iam_role.poll_libor_lambda_role.name}"
  policy_arn = "${aws_iam_policy.libor_api_s3_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "poll_libor_attach_athena_policy" {
  role       = "${aws_iam_role.poll_libor_lambda_role.name}"
  policy_arn = "${aws_iam_policy.libor_api_athena_policy.arn}"
}
