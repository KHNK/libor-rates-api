provider "aws" {
  profile = "${var.profile}"
  region  = "${var.default_region}"
}

variable "default_region" {
  default = "us-east-1"
}

variable "profile" {
  default = "default"
}
