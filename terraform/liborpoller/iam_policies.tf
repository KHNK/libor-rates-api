resource "aws_iam_policy" "libor_api_athena_policy" {
  name        = "libor-api-athena-policy"
  description = "Policy enabling access to athena"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "athena:*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "libor_api_s3_policy" {
  name        = "libor-api-s3-policy"
  description = "Policy enabling access to s3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "LambdaLiborBucketAccess",
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}
