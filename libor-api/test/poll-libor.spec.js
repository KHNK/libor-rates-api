const router = require('../src/functions/poll-libor');
const pollCtrl = require('../src/functions/poll-libor/poll-libor');
const pollEvent = require('../src/functions/poll-libor/event');
const { assert } = require('chai');
describe('poll-libor', () => {
  it('should poll global rates and return the appropriate shape of data', async () => {
    const { code, data } = await pollCtrl(pollEvent);
    assert.equal(code, 'POLL_SUCCESS');
  });

  it('should poll global rates in response to event', async () => {
    const { code, data } = await router.asyncHandler(pollEvent, {});
    assert.equal(code, 'POLL_SUCCESS');
    assert.isDefined(data, 'router did not return response');
    assert.typeOf(data, "string");
  });
});
