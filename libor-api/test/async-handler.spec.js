const asyncHandler = require('../src/lib/async-handler');
const sinon = require("sinon")
const { assert } = require("chai");
describe('async-handler', () => {
  it('should run a mocked handler', async () => {
    const spy = sinon.spy();
    const expectedData = 'success';
    await asyncHandler({}, {}, spy, async () => expectedData);
    console.log(spy.called)
    assert.isTrue(spy.calledOnce);
  });
});
