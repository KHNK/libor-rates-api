const s3 = require('../src/lib/s3');
const { assert } = require("chai");

const BUCKET = 'test-liborapi-bucket';

// TODO use replay
describe('s3', () => {
  it('should put an object in S3', async () => {
    const data = await s3.putObject(BUCKET, 'testFileName', "Label,0,1,2,3,4");
    assert.hasAllKeys(data,['ETag']);
  });
});
