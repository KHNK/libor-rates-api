const AWS = require('aws-sdk');

class S3Client {
  constructor () {
    this.s3 = new AWS.S3();
  }

  async putObject(Bucket, Key, Body) {
    return this.s3.putObject({
      Bucket,
      Key,
      Body
    }).promise();
  }
}

module.exports = new S3Client();