module.exports = async (event, ctx, callback, handler) => {
  try {
    const data = await handler(event, ctx);
    callback(null, data);
  } catch (e) {
    callback(e, {
      code: 'POLL_FAILURE',
      message: 'Daily Poll Failed.',
    });
  }
};
