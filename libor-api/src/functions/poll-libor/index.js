const asyncHandler = require('lib/async-handler');
const pollLibor = require('./poll-libor');
const s3 = require("../../lib/s3");
const AthenaHelper = require("./athena-helper");
const BUCKET = 'liborapi-daily-libor-rates';

const getFileName = (date) => `daily/${date}`;
/**
 * run script to create db and table
 */
const runDailyScript = async (date) => {
  await AthenaHelper.createDatabase();
  await AthenaHelper.createTable();
  // await AthenaHelper.createDailyPartition(date);
};

const pad = (value) => `0${value}`.slice(-2);

async function handler(event) {
  let data = null;
  const action = event["detail-type"];
  switch (action) {
    case 'Scheduled Event':
      const today = new Date();
      const date = `${today.getFullYear()}_${pad(today.getMonth())}_${pad(today.getDate())}`;
      const liborRates = await pollLibor(date);
      await s3.putObject(BUCKET, getFileName(date), liborRates.data);
      await runDailyScript(date);
      data = liborRates;
      break;
    default:
      data = {
        message: `Action ${action} is not supported`,
        code: 'ACTION_NOT_DEFINED',
      };
  }
  return data;
}

module.exports.asyncHandler = handler;
module.exports.handler = (event, context, callback) => asyncHandler(event, context, callback, handler);