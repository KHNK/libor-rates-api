const cheerio = require('cheerio');
const axios = require('axios');
const htmlparser2 = require('htmlparser2');

const ENDPOINT = 'http://www.global-rates.com/interest-rates/libor/libor.aspx';
// TODO
// - saved time using column id to find table
// since this function won't be executed many times a day
// we should use pull streams to ensure we are looking for a table of this shape
// so we are not relying on the id being consistent
const extractTableContent = ($) => {
  try {
    const rows = $('#lbl_hdr1').closest('table').children('tr');
    if (rows.length !== 16) {
      throw new Error('Table structure has changed');
    }

    const result = rows.map((i, el) => {
      if (i == 0) return null; // header row
      return $(el).children('td').map((idx, ele) => {
        if (idx === 0) { // first column is label
          return $(ele).find('a').text();
        }
        const rate = $(ele).text();
        return rate.replace('&nbsp;%', '');
      }).toArray();
    }).toArray();
    return result;
  } catch (e) {
    console.error(e);
  }
};

const isDailyRateAvailable = ($) => {
  const today = new Date().toDateString();
  try {
    const obj = $('#lbl_hdr2');
    const contentDate = new Date(obj.text()).toDateString();
    return (contentDate === today);
  } catch (e) {
    console.error(e);
  }
};

const formatCsvs = (date, flatLiborArray) => {
  try {
    let csvString = "";
    while(flatLiborArray.length > 0) {
      const maturityRow = flatLiborArray.splice(0, 6)
      const maturityRowString = maturityRow.splice(0,6).reduce((a,c)=>`${a},${c}`)
      csvString += `${maturityRowString},${date}\n`
    }
    return csvString;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

// We assume the trigger is only calling us on weekdays
async function pollLibor(date) {
  const { data } = await axios.get(ENDPOINT);
  const dom = htmlparser2.parseDOM(data, {});
  const cDom = cheerio.load(dom);
  if (!isDailyRateAvailable(cDom)) {
    // TODO schedule retry?, exit early
    console.log("Today's rate not found");
  }
  const tableContent = extractTableContent(cDom);
  const csv = formatCsvs(date, tableContent);
  return {
    code: 'POLL_SUCCESS',
    data: csv
  };

}

module.exports = pollLibor;
