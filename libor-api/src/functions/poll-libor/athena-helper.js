const AWS = require("aws-sdk");

class AthenaHelper {
  constructor () {
    this.DATABASE_NAME = "libor_rates_db";
    this.TABLE_NAME = "libor_daily";
    this.BASE_BUCKET = "s3://liborapi-daily-libor-rates";
    this.athenaClient = new AWS.Athena({region:'us-east-1'});
  }
  
  createDatabase() {
    const createDBQuery = `CREATE DATABASE IF NOT EXISTS \`${this.DATABASE_NAME}\``
    + " COMMENT 'Database For Daily Libor Rate CSV Dumps'"
    + ` LOCATION '${this.BASE_BUCKET}/db'`;
    const outputBucket = `${this.BASE_BUCKET}/db_output`;
    return this.runQuery(createDBQuery, outputBucket).catch((e)=>{console.error(e);console.error("Create Database Failed");});
  }
  

  createTable() {
    const createTableQuery = `CREATE EXTERNAL TABLE IF NOT EXISTS ${this.DATABASE_NAME}.${this.TABLE_NAME}(` +
    "  `label` string, " +
    "  `today` double, " +
    "  `prec1d` double, " +
    "  `prec2d` double, " +
    "  `prec3d` double, " +
    "  `prec4d` double, " + 
    "  `date` string)" +
    " ROW FORMAT DELIMITED " +
    "  FIELDS TERMINATED BY ','" + 
    " STORED AS INPUTFORMAT " +
    "  'org.apache.hadoop.mapred.TextInputFormat'" + 
    " OUTPUTFORMAT " +
    "  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'" +
    " LOCATION" +
    `  '${this.BASE_BUCKET}/daily/'` +
    " TBLPROPERTIES (" +
    "  'has_encrypted_data'='false', " +
    "  'transient_lastDdlTime'='1531718643')";
    const outputBucket = `${this.BASE_BUCKET}/table_output`;
    return this.runQuery(createTableQuery, outputBucket).catch((e)=>{console.error(e);console.error("Create Table Failed")});
  }

  // TODO why were partitions failing...
  // createDailyPartition(date) {
  //   const query=`ALTER TABLE ${this.DATABASE_NAME}.${this.TABLE_NAME} ` +
  //   ` ADD IF NOT EXISTS PARTITION(date='${date}') LOCATION 's3://liborapi-daily-libor-rates/daily/date=${date}/}'`;
  //   const outputBucket = `${this.BASE_BUCKET}/partition-output`;
  //   return this.runQuery(query, outputBucket).catch((e)=>{console.error(e);console.error("Create Daily Partition Failed")});
  // }

  runQuery(query, outputBucket, database = this.DATABASE_NAME) {
    return this.athenaClient.startQueryExecution({
      QueryString: query,
      ResultConfiguration: {
        OutputLocation: outputBucket
      }
    }).promise();
  }

  // waitQuery(queryId, retryCount) {
  //   // block
  //   const waiter = (timeMs) => {
  //     const waitUntil = new Date(Date.now() + timeMs);
  //   };

  //   const WAIT_INTERVAL = 1000; // block for 1 second each attempt to check query
  //   while(retryCount-- >= 0) {
  //     this.getQueryStatus();
  //   }
  // }

  // getQueryStatus(queryId) {
  //   return this.athenaClient.getQueryExecution().promise().catch((e)=>{})
  // }
}

module.exports = new AthenaHelper();
